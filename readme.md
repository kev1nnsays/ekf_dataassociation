![vp_nn_fullmap.png](https://bitbucket.org/repo/5ben8L/images/3640188566-vp_nn_fullmap.png)

# Overview #
This project applies the filters that I previously implemented in [extendedkf_unscentedkf_particleFilter.git](https://bitbucket.org/kev1nnsays/extendedkf_unscentedkf_particlefilter) to the commonly used **Victoria Park** dataset. 
Historically, this dataset was often used in the re-search community for benchmarking the performance of new SLAM algorithms. 
This dataset is realdata—it contains recorded velocity and steering angle information of a pickup truck instrumented with
a 2D laser scanner as it drove around a public park in Sydney. The park has many trees and
open space, and the laser scanner detected tree trunks to be used for perception. These detections will
serve as point features in your SLAM implementation. Here’s a URL where you can learn more: http:
//www-personal.acfr.usyd.edu.au/nebot/victoria_park.htm

**slamsim** is a modified version of the localization simulator used in extendedkf_unscentedkf_particleFilter. The number of land-
marks is now configurable and multiple range/bearing/markerId tuples are produced at each time step

**vicpark** contains the Victoria Park SLAM dataset.

# Implementation #
• slamsim/runsim.m – Main loop for simulator; should call ekfpredict sim() and ekfupdate()  
• vicpark/runvp.m–MainloopforVictoriaParkdataset; shouldcallekfpredict vp()andekfupdate()  
• ekfpredict sim.m – EKF prediction for simulator process model  
• ekfpredict vp.m – EKF prediction for Victoria Park process model  
• ekfupdate.m – EKF landmark observation update; used by both simulator and Victoria Park  
• initialize new landmark.m – augments the state vector with a new landmark element, called by  
ekfupdate()
• da known.m – performs known data association, called by ekfupdate(), can be used in simulator  
• da nn.m – performs incremental maximum likelihood data association, called by ekfupdate(), can be  
used in simulator and Victoria Park
• da jcbb.m – performs joint-compatibility branch and bound data association, called by ekfupdate(),  
can be used in simulator and Victoria Park  

### slamsim (essentially, the same files from extendedkf_unscentedkf_particleFilter.git) ###  
• slamsim/deg2rad.m – degrees to radians  
• slamsim/generateMotion.m – simulates simple motion commands  
• slamsim/generateScript.m – generates data according to initial mean and noise parameters  
• slamsim/prediction.m – move robot according to specified motion  
• slamsim/observation.m – generates noise free observation model  
• slamsim/sampleOdometry.m – implements Table 5.6  
• slamsim/sample.m – samples from a covariance matrix  
• slamsim/meanAndVariance.m – returns the mean and variance for a set of non-weighted samples  
(illustrates handling of angles)  
• slamsim/getfieldinfo.m – gets field information  
• slamsim/minimizedAngle.m – normalizes an angle to [−π,π]  
• slamsim/plotcircle.m – draws a circle  
• slamsim/plotcov2d.m – draws a 2-D covariance matrix  
• slamsim/plotfield.m – draws the field with landmarks  
• slamsim/plotmarker.m – draws a dot at a specified 2D point (useful for plotting samples)  
• slamsim/plotrobot.m – draws the robot  
• slamsim/plotSamples.m – plots particles from the PF  
 
### vicpark (files for Victoria Park dataset) ###  
• vicpark/aa3 dr.mat – dead-reckoned vehicle data  
• vicpark/aa3 gpsx.mat – GPS data for ground-truthing  
• vicpark/aa3 lsr2.mat – 2D laser scanner data  
• detectTreesI16.m – routine for detecting tree-trunk feature points from laser data  
• load vp si.m – function that loads and converts the Victoria Park .mat files into a single structure  
variable called Data  
• plotbot.m – draws the robot as a triangle  
• viewLsr.m – script for graphically displaying the laser data