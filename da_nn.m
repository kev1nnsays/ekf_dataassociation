function [Li]=da_nn(z, R)
% perform nearest-neighbor data association

global Param;
global State;

chi = 3.5;
Li = [];
totalNumNewLM = 0;

if State.Ekf.nL == 0 
    for i = 1:size(z,2)
        initial_LM = [z(:,1); i]; %set first landmark to id #1
        initialize_new_landmark(initial_LM);
        Li(1,i) = State.Ekf.nL;
    end
else
    for i = 1:size(z, 2)
        for j = 1:State.Ekf.nL %iterate through pre-existing landmarks
             P_BF = [State.Ekf.Sigma(1:3,1:3), State.Ekf.Sigma(1:3,3+2*j-1:3+2*j);
             State.Ekf.Sigma(3+2*j-1:3+2*j,1:3),State.Ekf.Sigma(3+2*j-1:3+2*j, 3+2*j-1:3+2*j)];

            index_x = State.Ekf.iL{j}(1); %find the pertainant indexes in mu
            index_y = State.Ekf.iL{j}(2);
            delta_x = State.Ekf.mu(3+index_x,1) - State.Ekf.mu(1,1); %belief x range to LM
            delta_y = State.Ekf.mu(3+index_y,1) - State.Ekf.mu(2,1); %belief y range to LM
            
            delta = [delta_x; %x
                     delta_y];%y
            q = delta'*delta; %belief squared range
            zhat = [sqrt(q);
                    minimizedAngle(atan2(delta_y, delta_x)-State.Ekf.mu(3,1))]; %belief/est. observation (ie. h)
            H = (1/q)*[-sqrt(q)*delta_x, -sqrt(q)*delta_y, 0, sqrt(q)*delta_x, sqrt(q)*delta_y;... %2x5
                        delta_y        , -delta_x        ,-q, -delta_y       , delta_x        ]; %Jacobian of h

%             P_BF = State.Ekf.Sigma;
%             F = [[eye(3); zeros(2,3)],[zeros(5,2*j - 2)],[zeros(3,2);eye(2)],[zeros(5,2*State.Ekf.nL - 2*j)]]; %5x(3+2n)
%             H = (1/q)*[-sqrt(q)*delta_x, -sqrt(q)*delta_y, 0, sqrt(q)*delta_x, sqrt(q)*delta_y;... %2x5
%                         delta_y        , -delta_x        ,-q, -delta_y       , delta_x        ]*F; %Jacobian of h
            
            P_ij = H * P_BF * H' + R;

            E = (z(:,i) - zhat); %actual observation - predicted meas for pre-existing LM
            E(1,1) = abs(E(1,1));
            E(2,1) = minimizedAngle(E(2,1));
            Mahal_sq_array(j,1) = E'*inv(P_ij)*E;
        end
        [Mahal_sq, signature] = min(Mahal_sq_array); %find the least distance
        
        if Mahal_sq < chi^2 %associate with pre-existing LM
            Li(1,i) = signature;
%             fprintf('Assigning observation %d to %d \n', i, signature);
        else
            totalNumNewLM = totalNumNewLM + 1;
            Li(1,i) = State.Ekf.nL + totalNumNewLM; %associate as new LM
%             fprintf('Making new landmark %d\n', Li(1,i))
        end
    end
end
end