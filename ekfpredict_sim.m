function ekfpredict_sim(u) 
% EKF-SLAM prediction for simulator process model

%Param.initialStateMean, numSteps, Param.maxObs, Param.alphas, Param.beta,
%Param.deltaT, Param.R
global Param;

% State.Ekf.mu = Param.initialStateMean;
% State.Ekf.Sigma = zeros(3);
global State;

%Predict mu
F = [eye(3), zeros(3, 2*State.Ekf.nL)]; %3x(3+2n)
drot1 = u(1);
dtrans = u(2);
drot2 = u(3);
theta = State.Ekf.mu(3,1);
motion(1,1) = dtrans * cos(theta + drot1); %x
motion(2,1) = dtrans * sin(theta + drot1); %y
motion(3,1) = drot1 + drot2; %theta
State.Ekf.mu = State.Ekf.mu + F'*motion; %mu belief
State.Ekf.mu(3,1) = minimizedAngle(State.Ekf.mu(3,1));


%Predict Sigma
M = [Param.alphas(1)*u(1)^2 + Param.alphas(2)*u(2)^2, 0, 0; %Process noise
    0, Param.alphas(3)*u(2)^2 + Param.alphas(4)*u(1)^2+Param.alphas(4)*u(3)^2, 0;
    0, 0, Param.alphas(1)*u(3)^2 + Param.alphas(2)*u(2)^2]; 
G = eye(size(F,2)) + F'* [0,0, -dtrans*sin(theta+drot1);... %deriv. of process model
                          0,0, dtrans*cos(theta+drot1);...
                          0,0,0;]*F;             
State.Ekf.Sigma = G * State.Ekf.Sigma * G' + F' * M * F ; %sigma belief

end