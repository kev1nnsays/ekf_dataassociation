function initialize_new_landmark(z)

global Param;
global State;

r = z(1,1);
phi = z(2,1);
theta = State.Ekf.mu(3,1);
new = [State.Ekf.mu(1,1) + r*cos(phi + theta);
       State.Ekf.mu(2,1) + r*sin(phi + theta)];
State.Ekf.mu = [State.Ekf.mu; new]; %Augment mu
% State.Ekf.mu(3,1) = minimizedAngle(State.Ekf.mu(3,1));

State.Ekf.Sigma = [State.Ekf.Sigma,zeros(size(State.Ekf.Sigma,1),1),zeros(size(State.Ekf.Sigma,1),1)]; %append column of zeros
State.Ekf.Sigma = [State.Ekf.Sigma;zeros(1,size(State.Ekf.Sigma,2));zeros(1,size(State.Ekf.Sigma,2))]; %append row of zeros
State.Ekf.Sigma(end-1,end-1) = 10^3; %Augment Sigma, init new LM at infinite variance
State.Ekf.Sigma(end,end) = 10^3;

State.Ekf.iM = [State.Ekf.iM, z(3,1), z(3,1)]; % append signature to 2*nL vector containing map indices
State.Ekf.iL = [State.Ekf.iL, [length(State.Ekf.iM)-1,length(State.Ekf.iM)]]; %nL cell array holding indices of landmark i
State.Ekf.sL = [State.Ekf.sL, z(3,1)]; %add signature to sL: nL vector signature of LM
State.Ekf.nL = State.Ekf.nL +1; %scalar # of landmarks

% fprintf('Initialized. Now at (%d)\n', State.Ekf.nL);
% fprintf('iM. Now at (%d)\n', size(State.Ekf.iM));

end
