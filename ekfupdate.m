function ekfupdate(z)
% EKF-SLAM update step for both simulator and Victoria Park data set
%z: 3xn

global Param;
global State;

% returns state vector indices pairing observations with landmarks
switch lower(Param.dataAssociation)
    case 'known'
        Li = da_known(z(3,:));
    case 'nn'
        Li = da_nn(z(1:2,:), Param.R);
    case 'jcbb'
        Li = da_jcbb(z(1:2,:), Param.R);
    otherwise
        error('unrecognized data association method: "%s"', Param.dataAssociation);
end

%associate the observations with results from data-association
z(3,:) = Li;

method = 'sequential';
switch method
    case 'sequential'
        for i = 1:size(z,2)%for all observed features
            j = find(State.Ekf.sL == z(3,i)); %Check where LM is in dictionary, returns index of where signature is found
            if isempty(j) %if observation is new landmark, augment state vector
                initialize_new_landmark(z(:,i));
                j = State.Ekf.nL; 
            end %augment matrix
            index_x = State.Ekf.iL{j}(1); %find the pertainant indexes in mu
            index_y = State.Ekf.iL{j}(2);
            delta_x = State.Ekf.mu(3+index_x,1) - State.Ekf.mu(1,1); %belief x range to LM
            delta_y = State.Ekf.mu(3+index_y,1) - State.Ekf.mu(2,1); %belief y range to LM
            delta = [delta_x; %x
                     delta_y];%y
            q = delta'*delta; %belief squared range
            zhat = [sqrt(q);
                    minimizedAngle(atan2(delta_y, delta_x)-State.Ekf.mu(3,1))]; %belief/est. observation (obs. model h) 
            F = [[eye(3); zeros(2,3)],[zeros(5,2*j - 2)],[zeros(3,2);eye(2)],[zeros(5,2*State.Ekf.nL - 2*j)]]; %5x(3+2n)
            H = (1/q)*[-sqrt(q)*delta_x, -sqrt(q)*delta_y, 0, sqrt(q)*delta_x, sqrt(q)*delta_y;... %2x(3+2n)
                        delta_y        , -delta_x        ,-q, -delta_y       , delta_x        ]*F;

%             H = 1/(sqrt(q))*[-delta_x, -delta_y, 0, delta_x, delta_y;...
%                             delta_y/q, -delta_x/q, -1, -delta_y/q, delta_x/q]*F; 

            K = State.Ekf.Sigma*H'*inv(H*State.Ekf.Sigma*H'+Param.R);
            State.Ekf.mu = State.Ekf.mu+K*(z(1:2,i)-zhat); %corrected mu belief
            State.Ekf.mu(3,1) = minimizedAngle(State.Ekf.mu(3,1));
            State.Ekf.Sigma = (eye(size(K,1))-K*H)*State.Ekf.Sigma; %corrected sigma belief
        end
    case 'batch'
         %initialize all new landmarks
         for i = 1:size(z,2)%for all observed features
            j = find(State.Ekf.sL == z(3,i)); %Check where LM is in dictionary, returns index of where signature is found
            if isempty(j) %if observation is new landmark, augment state vector
                initialize_new_landmark(z(:,i));
            end
         end
         
         %initialize matrices
         H = [];
         Q = Param.R;
         zhat = [0;0];
         z_concatenate = [0;0];

         %update the initialized matrices using observations
         for i = 1:size(z,2)%for all observed features
            j = find(State.Ekf.sL == z(3,i)); %Check where LM is in dictionary, returns index of where signature is found
                index_x = State.Ekf.iL{j}(1); %find the pertainant indexes in mu
                index_y = State.Ekf.iL{j}(2);
                delta_x = State.Ekf.mu(3+index_x,1) - State.Ekf.mu(1,1); %belief x range to LM
                delta_y = State.Ekf.mu(3+index_y,1) - State.Ekf.mu(2,1); %belief y range to LM
                delta = [delta_x; %x
                         delta_y];%y
                q = delta'*delta; %belief squared range

                F = [[eye(3); zeros(2,3)],[zeros(5,2*j - 2)],[zeros(3,2);eye(2)],[zeros(5,2*State.Ekf.nL - 2*j)]]; %5x(3+2n)
                if i == 1
                    z_concatenate = [z(1,i);z(2,i)];
                    zhat = [sqrt(q);
                            minimizedAngle(atan2(delta_y, delta_x)-State.Ekf.mu(3,1))]; %belief/est. observation (obs. model h)
                    H = (1/q)*[-sqrt(q)*delta_x, -sqrt(q)*delta_y, 0, sqrt(q)*delta_x, sqrt(q)*delta_y;... %stack 2x(3+2n) 
                            delta_y        , -delta_x        ,-q, -delta_y       , delta_x        ]*F;
                else
                    z_concatenate = [z_concatenate; z(1,i);z(2,i)]; %concatenate all observations
                    zhat = [zhat; 
                            sqrt(q);
                            minimizedAngle(atan2(delta_y, delta_x)-State.Ekf.mu(3,1))]; %belief/est. observation
                    H = [H; ... %Jacobian of 
                        (1/q)*[-sqrt(q)*delta_x, -sqrt(q)*delta_y, 0, sqrt(q)*delta_x, sqrt(q)*delta_y;... %stack 2x(3+2n) 
                            delta_y        , -delta_x        ,-q, -delta_y       , delta_x        ]*F];
                    Q = [Q; zeros(2,size(Q,2))]; %add 2 rows of zeros
                    Q = [Q, zeros(size(Q,1),2)]; %add 2 columns of zeros
                    Q(end-1:end,end-1:end) = Param.R;  %concatenate R
                end
         end
        
        %perform prediction update in batch
        K = State.Ekf.Sigma*H'*inv(H*State.Ekf.Sigma*H'+Q); %(3+2n) x 2n
        State.Ekf.mu = State.Ekf.mu+K*( z_concatenate - zhat); %corrected mu belief
        State.Ekf.mu(3,1) = minimizedAngle(State.Ekf.mu(3,1));
        State.Ekf.Sigma = (eye(size(K,1))-K*H)*State.Ekf.Sigma; %corrected sigma belief
        
end %switch end

        
end
