function ekfpredict_vp(u, dt)
% EKF-SLAM prediction for Victoria Park process model

global Param;
global State;

%Predict mu
F = [eye(3), zeros(3, 2*State.Ekf.nL)]; %3x(3+2n)
ve = u(1,1);
alpha = u(2,1);
vc = ve/(1-tan(alpha)*(Param.H/Param.L));
phi = State.Ekf.mu(3,1);
motion(1,1) = dt*(vc*cos(phi)-(vc/Param.L)*tan(alpha)*(Param.a*sin(phi)+Param.b*cos(phi)))+ Param.Qu(1,1); %x
motion(2,1) = dt*(vc*sin(phi)+(vc/Param.L)*tan(alpha)*(Param.a*cos(phi)-Param.b*sin(phi)))+ Param.Qu(1,1); %y 
motion(3,1) = dt*(vc/Param.L)*tan(alpha) + Param.Qu(2,2); %theta
State.Ekf.mu = State.Ekf.mu + F'*motion; %mu belief
State.Ekf.mu(3,1) = minimizedAngle(State.Ekf.mu(3,1));


%Predict Sigma
G = eye(size(F,2)) + F'* [0,0, -dt*(vc*sin(phi)+(vc/Param.L)*tan(alpha)*(Param.a*cos(phi)-Param.b*sin(phi)));... %deriv. of process model
                          0,0, dt*(vc*cos(phi)-(vc/Param.L)*tan(alpha)*(Param.a*sin(phi)+Param.b*cos(phi)));...
                          0,0,0;]*F;             
State.Ekf.Sigma = G * State.Ekf.Sigma * G' + F' * Param.Qf * F ; %sigma belief
end